import browser from "webextension-polyfill";

const selection = getSelection();
const activeElement = document.activeElement;

if (
  (activeElement instanceof HTMLInputElement || activeElement instanceof HTMLTextAreaElement) &&
  activeElement.selectionStart !== activeElement.selectionEnd
) {
  // If we're currently focusing a text field,
  // (de-)obfuscate that text field:
  activeElement.classList.toggle("obfuscate-extension-obfuscated");
  browser.runtime.sendMessage("obfuscateSelection");
} else if (selection?.type === "Range") {
  const nodes = new Array(selection.rangeCount)
    .fill(0)
    .map((_, i) => selection.getRangeAt(i))
    .map(range => range.commonAncestorContainer);

  nodes.forEach(node => {
    let element = (node instanceof HTMLElement || node instanceof SVGElement)
      ? node
      : node.parentElement;
    if (element instanceof HTMLElement || element instanceof SVGElement) {
      element.classList.toggle("obfuscate-extension-obfuscated");
    }
  });
  browser.runtime.sendMessage("obfuscateSelection");
} else {
  if (
    ['"Flow Circular"', '"Flow Rounded"', '"Flow Block"']
      .includes(getComputedStyle(document.body).fontFamily)
  ) {
    browser.runtime.sendMessage("reveal");
  } else {
    browser.runtime.sendMessage("obfuscate");
  }
}
