import browser from "webextension-polyfill";

browser.action?.onClicked.addListener(() => {
  toggle();
});

// browser.commands is not supported on Firefox for Android
if (typeof browser.commands !== "undefined") {
  browser.commands.onCommand.addListener(function (command) {
    if (command === "obfuscate" || command === "_execute_page_action" || command === "_execute_action") {
      // The "special shortcut" `_execute_page_action` should be sufficient
      // (see https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/commands#special_shortcuts),
      // but since there's no more distinction between page and browser actions in
      // Manifest V3, I've added an explicit command to remain forward compatible:
      toggle();
    }
  });
}

browser.runtime.onMessage.addListener(async (message, _sender) => {
  if (message === "reveal") {
    reveal();
  } else if (message === "obfuscate") {
    obfuscate();
  } else if (message === "obfuscateSelection") {
    obfuscateSelection();
  }
});

browser.runtime.onInstalled.addListener(() => {
  if (typeof browser.menus !== "undefined") {
    browser.menus.create({
      command: "_execute_action",
      contexts: ["selection"],
      id: "obfuscate-selection",
      // Unfortunately we need host permissions to see whether the selection is
      // already obfuscated, which isn't really worth it just to dynamically update
      // the context menu name:
      title: "Obfuscate/reveal selection",
      viewTypes: ["tab"],
    });
  }
});

async function toggle() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to toggle without a tab being active.");
    return;
  }

  // This script will send us a message telling us to obfuscate or reveal,
  // depending on the current font family. See `browser.runtime.onMessage` above.
  browser.scripting.executeScript({
    files: ["/dist/content.js"],
    target: { tabId: activeTabs[0].id },
  })
}

async function obfuscate() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to obfuscate without a tab being active.");
    return;
  }
  browser.scripting.insertCSS({
    files: ["/src/obfuscate.css"],
    target: {
      allFrames: true,
      tabId: activeTabs[0]?.id,
    },
  });
}

async function obfuscateSelection() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to obfuscate without a tab being active.");
    return;
  }
  browser.scripting.insertCSS({
    files: ["/src/obfuscate-selection.css"],
    target: {
      allFrames: true,
      tabId: activeTabs[0]?.id,
    },
  });
}

async function reveal() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to de-obfuscate without a tab being active.");
    return;
  }
  browser.scripting.removeCSS({
    files: ["/src/obfuscate.css"],
    target: {
      allFrames: true,
      tabId: activeTabs[0]?.id,
    },
  });
}
