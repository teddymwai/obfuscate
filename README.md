Sometimes you might want to share a screenshot of the website you're on, without
revealing the personal data that is visible at that time.
With Obfuscate, you can make text unreadable without changing the structure of
the web page.

Hit the extension button or press Alt+Shift+O to activate for the current page.
Hit it again to de-activate. Select text before obfuscating to only obfuscate
(roughly) that part of the page.

# Permissions

Obfuscate requests the following permissions:

- `activeTab`: Used to change the active tab, obfuscating its contents.
